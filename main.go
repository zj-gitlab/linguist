package main

import (
	"flag"
	"fmt"
	"os"
	"runtime/pprof"

	"gitlab.com/zj/linguist/detector"
)

var repo = flag.String("repo", "", "A git repo to analyse")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			fmt.Println(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if *repo == "" {
		os.Exit(1)
	}

	x := detector.NewAnalyses(*repo)
	x.Analyze()
	fmt.Println(x)
}
