## linguist

A port of GitHubs [linguist](https://github.com/github/linguist) to Go.

However, it does not contain the following features:
- Detection for generated files, e.g. compiled JS from CoffeeScript
- Tokenization when no file type is detected yet
- Modeline detection strategy
- No bayesian classification
- No caching of results (yet)
- No overriding detection using `.gitattributes`
- Decent binary file detection

## How to use it

Provided a [usual golang setup](https://golang.org/doc/code.html#Organization)
a `go install` will do:

`go install gitlab.com/zj/linguist`

After which it can be called from your terminal:

```bash
linguist -repo /path/to/a/bare/repo
```
If you get a error by git telling you its not a valid repository, try appending
`.git` to the path.

## Development

Needed to develop against this is needed:
1. go, version 1.8 is recommended
1. ruby, version 2.3 and up is needed (only to generate language_definitions.go)
1. git, both as development VCS and to get the file from the passed `-repo`
