package testhelper

import (
	"fmt"
	"os"
)

// Returns the path to the repository of this project. If one is to perform
// write operations of any kind, reimplement this to copy the repository somewhere
// in /tmp
func RepoPath() string {
	buildDir, present := os.LookupEnv("CI_PROJECT_DIR")

	if present {
		return fmt.Sprintf("%s/.git", buildDir)
	} else {
		goPath, present := os.LookupEnv("GOPATH")
		if !present {
			fmt.Println("Not a valid GOPATH is set up")
			return ""
		}

		return fmt.Sprintf("%s/src/gitlab.com/zj/linguist/.git", goPath)
	}
}
