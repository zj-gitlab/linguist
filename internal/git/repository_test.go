package git

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/zj/linguist/internal/testhelper"
)

func TestRawTree(t *testing.T) {
	repo := NewRepository(testhelper.RepoPath())

	tree, err := repo.RawTree("HEAD")
	require.NoError(t, err)
	assert.NotEmpty(t, tree)

	match, err := regexp.Match("LICENSE\n", tree)
	require.NoError(t, err)
	assert.True(t, match)
}

func TestRawTreeInvalidRepo(t *testing.T) {
	repo := NewRepository("/tmp/not_a_git_dir")

	tree, _ := repo.RawTree("HEAD")
	assert.Empty(t, tree)
}

func TestLoadBlobOid(t *testing.T) {
	// oid from the LICENSE file
	oid := "69fafd082518059f122488d2dd7402c79d3c9dc2"

	path := testhelper.RepoPath()
	repo := NewRepository(path)

	// Load the full blob
	buff, err := repo.LoadBlobOid(&oid, 0)
	require.NoError(t, err)
	assert.Equal(t, 1077, len(buff))

	// Load only 5 bytes
	buff, err = repo.LoadBlobOid(&oid, 5)
	require.NoError(t, err)
	assert.Equal(t, 5, len(buff))
}
