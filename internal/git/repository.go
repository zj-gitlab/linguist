package git

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"os/exec"
)

type Repository struct {
	Path string
}

func NewRepository(path string) *Repository {
	return &Repository{Path: path}
}

// Returns a []byte with the full, recurive tree for a `treeish` object.
// Usually this will be `HEAD`.
// The byte slice will consist of lines structured like:
// 100644 blob 82efa4a1367795cebb4fee45dacf0ddc641d4a91   21012   Gemfile
func (r *Repository) RawTree(treeish string) ([]byte, error) {
	stdinReader, stdinWriter := io.Pipe()
	cmdArgs := []string{
		"--git-dir", r.Path,
		"ls-tree",
		"--long", // Displays the bytes per file
		"-r",     // recursive expand the trees
		treeish,
	}

	cmd, err := NewCommand(exec.Command("git", cmdArgs...), stdinReader, nil, nil)
	if err != nil {
		return nil, err
	}
	defer cmd.Kill()
	defer stdinWriter.Close()
	defer stdinReader.Close()

	buff, err := ioutil.ReadAll(cmd)
	if len(buff) == 0 {
		return nil, errors.New("no tree could be found, is this a valid git repository?")
	}

	return buff, err
}

// Passed a blob oid, the contents will be requested and copied to a []byte which
// will be returned. The maximal buffer capacity is any valid uint, where 0 means
// no loading limit
func (r *Repository) LoadBlobOid(oid *string, buffSize uint) ([]byte, error) {
	stdinReader, stdinWriter := io.Pipe()
	cmdArgs := []string{"--git-dir", r.Path, "cat-file", "-p", *oid}
	cmd, err := NewCommand(exec.Command("git", cmdArgs...), stdinReader, nil, nil)
	if err != nil {
		return nil, err
	}

	defer cmd.Kill()
	defer stdinWriter.Close()
	defer stdinReader.Close()

	stdout := bufio.NewReader(cmd)
	var buff []byte

	if buffSize == 0 {
		return ioutil.ReadAll(cmd)
	} else {
		buff = make([]byte, buffSize)
		_, err := stdout.Read(buff)
		return buff, err
	}
}

func (r *Repository) String() string {
	return r.Path
}
