package detector

import (
	"bytes"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/zj/linguist/internal/git"
	"gitlab.com/zj/linguist/languages"
)

type Blob struct {
	Repo     *git.Repository
	Filename *string
	ByteSize int64
	Oid      *string
	dirs     []string
	buffer   []byte
}

// Return what was detected, for how many bytes and what guesser got it
type Detection struct {
	Language *languages.Language
	Size     int64
}

// When detecting the MIME type, its advised to only read the first
// 512 bytes: https://golang.org/pkg/net/http/#DetectContentType
const maxBufferSize = 512

var interpreterRegex = regexp.MustCompile(`\A#!/usr/s?bin/(env ((bin/)?(?P<env>\w+))|(?P<bin>\w+))`)

var guessers = []func(*Blob) []*languages.Language{
	guessByExtension,
	guessByFilename,
	guessByInterpreter,
	guessByMIMEtype,
}

func NewBlob(repo *git.Repository, path, oid *string, bytes int64) *Blob {
	dirs, filename := splitPath(path)
	return &Blob{Repo: repo, Filename: filename, dirs: dirs, ByteSize: bytes, Oid: oid}
}

func (b *Blob) Extension() string {
	return filepath.Ext(*b.Filename)
}

func (b *Blob) Detect() *Detection {
	var previousGuesses []*languages.Language
	var bestGuess *languages.Language

	for _, guesser := range guessers {
		guesses := guesser(b)
		if len(guesses) == 1 {
			return &Detection{Size: b.ByteSize, Language: guesses[0]}
		}

		if len(guesses) > 1 {
			bestGuess = guesses[0]
			previousGuesses = combine(guesses, previousGuesses)
		}

		if len(previousGuesses) == 1 {
			return &Detection{Size: b.ByteSize, Language: previousGuesses[0]}
		}
		if len(previousGuesses) > 1 {
			bestGuess = previousGuesses[0]
		}
	}

	return &Detection{Size: b.ByteSize, Language: bestGuess}
}

func guessByFilename(blob *Blob) []*languages.Language {
	return languages.LanguageByFileName(*blob.Filename)
}

func guessByExtension(blob *Blob) []*languages.Language {
	return languages.LanguageByExtension(blob.Extension())
}

func guessByInterpreter(blob *Blob) []*languages.Language {
	interpreter, err := blob.Interpreter()
	if err != nil {
		log.WithFields(log.Fields{"repo": blob.Repo, "oid": blob.Oid}).Errorf("error reading git blob: %v\n", err)
		return nil
	}

	return languages.LanguageByInterpreter(interpreter)
}

func guessByMIMEtype(blob *Blob) []*languages.Language {
	mime, err := blob.MIMEType()
	if err != nil {
		log.WithFields(log.Fields{"repo": blob.Repo, "oid": blob.Oid}).Errorf("error reading git blob: %v\n", err)
		return nil
	}

	if mime == "application/octet-stream" {
		return nil
	}

	return languages.LanguageByMimeType(mime)
}

func combine(a, b []*languages.Language) []*languages.Language {
	var out []*languages.Language

	if b == nil {
		return a
	}

	for _, langA := range a {
		for _, langB := range b {
			if langA == langB {
				out = append(out, langA)
			}
		}
	}

	return out
}

func (b *Blob) Interpreter() (interpreter string, err error) {
	err = b.loadBuffer()
	if err != nil {
		return
	}

	if !bytes.HasPrefix(b.buffer, []byte("#!")) {
		return
	}

	tryMatch := interpreterRegex.FindAllSubmatch(b.buffer, -1)
	if tryMatch == nil {
		return
	}

	match := tryMatch[0]
	names := interpreterRegex.SubexpNames()

	paramsMap := make(map[string]string)
	for i, n := range match {
		paramsMap[names[i]] = string(n)
	}

	interpreter = paramsMap["env"]

	if interpreter != "" {
		return
	}

	return paramsMap["bin"], nil
}

func (b *Blob) MIMEType() (mime string, err error) {
	err = b.loadBuffer()
	if err != nil {
		return
	}

	byteMime := http.DetectContentType(b.buffer)
	mime = string(byteMime)
	return
}

func (b *Blob) loadBuffer() error {
	if b.buffer != nil {
		return nil
	}

	buff, err := b.Repo.LoadBlobOid(b.Oid, maxBufferSize)
	b.buffer = buff
	return err
}

func (b *Blob) SkipAnalytics() bool {
	if b.ByteSize == 0 || 1<<10<<10 < b.ByteSize {
		return true
	}

	if b.skipDirectory() || b.skipFile() {
		return true
	}

	return false
}

var (
	dirBlacklist = []*regexp.Regexp{
		regexp.MustCompile(`\A[Dd]ocs?`),
		regexp.MustCompile(`\A[Dd]ocumentation`),
		regexp.MustCompile(`\A[Jj]avadoc`),
		regexp.MustCompile(`\A[Mm]an`),
		regexp.MustCompile(`\A[Ee]xamples`),
		regexp.MustCompile(`\A[Dd]emos?`),
		regexp.MustCompile(`\A[Ss]amples?`),
		regexp.MustCompile(`\Avendor`),
	}

	fileBlacklist = []*regexp.Regexp{
		regexp.MustCompile(`(?i)\ACHANGE(S|LOG)?`),
		regexp.MustCompile(`(?i)\ACONTRIBUTING`),
		regexp.MustCompile(`(?i)\ACOPYING`),
		regexp.MustCompile(`(?i)\ACONTRIBUTING(\.|$)`),
		regexp.MustCompile(`(?i)\AINSTALL`),
		regexp.MustCompile(`(?i)\ALICEN[CS]E`),
		regexp.MustCompile(`(?i)\AREADME`),
		regexp.MustCompile(`\.lock\z`),
	}
)

func (b *Blob) skipDirectory() bool {
	for _, dir := range b.dirs {
		for _, regex := range dirBlacklist {
			if regex.MatchString(dir) {
				return true
			}
		}
	}

	return false
}

func (b *Blob) skipFile() bool {
	for _, regex := range fileBlacklist {
		if regex.MatchString(*b.Filename) {
			return true
		}
	}

	return false
}

func splitPath(path *string) (dirs []string, filename *string) {
	components := strings.Split(*path, string(os.PathSeparator))
	dirs = components[:len(components)-1]
	filename = &components[len(components)-1]

	return
}

func (b *Blob) setBuffer(str string) {
	b.buffer = make([]byte, 1)
	b.buffer = []byte(str)
}
