package detector

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDetectByFilename(t *testing.T) {
	testCases := []struct {
		filename string
		language string
	}{
		{"file.rb", "Ruby"},
		{"main.go", "Go"},
		{"data.json", "JSON"},
		{"Gemfile", "Ruby"},
	}

	for _, tc := range testCases {
		blob := NewBlob(nil, &tc.filename, nil, 1)

		assert.Equal(t, tc.language, blob.Detect().Language.Name)
	}
}

func TestExtentionExtraction(t *testing.T) {
	testCases := []struct {
		filename  string
		extension string
	}{
		{"file.rb", ".rb"},
		{"file", ""},
	}

	for _, tc := range testCases {
		blob := &Blob{Filename: &tc.filename}
		assert.Equal(t, tc.extension, blob.Extension())
	}
}

func TestExtractInterpreter(t *testing.T) {
	testCases := []struct {
		Buff        string
		Interpreter string
	}{
		{"#!/usr/bin/env ruby", "ruby"},
		{"#!/usr/bin/ruby1", "ruby1"},
		{"#!/usr/sbin/go", "go"},
		{"#!/usr/bin/env bin/crystal", "crystal"},
		{"#!/usr/bin/env crystal", "crystal"},

		{" #!/usr/bin/ruby", ""},
		{" ", ""},
		{"#!test", ""},
		{"foo", ""},
	}

	for _, tc := range testCases {
		blob := Blob{buffer: []byte(tc.Buff)}

		interpreter, err := blob.Interpreter()

		require.NoError(t, err)
		assert.Equal(t, tc.Interpreter, interpreter)
	}
}

func TestSkipAnalytics(t *testing.T) {
	ph := "placeholder"

	// Its skips small and big files
	emptyBlob := NewBlob(nil, &ph, nil, 0)
	assert.True(t, emptyBlob.SkipAnalytics())

	bigBlob := NewBlob(nil, &ph, nil, 10000000000000)
	assert.True(t, bigBlob.SkipAnalytics())

	// It tests the path to decide on ignoring
	testCases := []struct {
		path string
		skip bool
	}{
		{"CHANGE", true},
		{"CHANGELOG", true},
		{"changelog", true},
		{"/some/path/CHANGE", true},
		{"/some/path/INSTALL", true},
		{"Gemfile.lock", true},
		{"doc/file.rb", true},
		{"Man/file", true},
		{"vendor/zj/linguist/main.go", true},
		{"samples/zj/linguist/main.go", true},

		{"/some/path/some_file", false},
		{"blob_test.go", false},
		{"blob_test.go", false},
	}

	for _, tc := range testCases {
		blob := NewBlob(nil, &tc.path, nil, 1)

		assert.Equal(t, tc.skip, blob.SkipAnalytics(), tc.path)
	}
}
