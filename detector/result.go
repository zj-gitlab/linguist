package detector

import (
	"fmt"
	"sort"

	"gitlab.com/zj/linguist/languages"
)

type Result struct {
	results map[*languages.Language]int64
}

func NewResult() Result {
	return Result{results: make(map[*languages.Language]int64)}
}

func (r *Result) Add(detection *Detection) {
	lang := detection.Language

	if lang == nil {
		return
	}

	// We're detecting markup and programming languages :)
	if lang.Type != "data" && lang.Type != "prose" {
		r.results[lang] += detection.Size
	}
}

type ResultTuple struct {
	Language   string
	TotalBytes int64
}

type LanguageResultList []ResultTuple

func (l LanguageResultList) Len() int           { return len(l) }
func (l LanguageResultList) Less(i, j int) bool { return l[i].TotalBytes < l[j].TotalBytes }
func (l LanguageResultList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }

// Hacky, but needed to have some output now to make debugging etc much easier
func (r *Result) String() string {
	langSlice := make(LanguageResultList, len(r.results))

	i := 0
	for k, v := range r.results {
		langSlice[i] = ResultTuple{k.Name, v}
		i++
	}

	sort.Sort(sort.Reverse(langSlice))

	str := "Result:\n"
	for _, lang := range langSlice {
		str += fmt.Sprintf("Detected %s for %v bytes\n", lang.Language, lang.TotalBytes)
	}

	return str
}
