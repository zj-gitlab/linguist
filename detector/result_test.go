package detector

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/zj/linguist/languages"
)

func TestResult(t *testing.T) {
	result := NewResult()
	ruby := languages.LanguageByExtension(".rb")[0]
	golang := languages.LanguageByExtension(".go")[0]

	result.Add(&Detection{ruby, 8})
	result.Add(&Detection{golang, 4})

	resultString := "Result:\nDetected Ruby for 8 bytes\nDetected Go for 4 bytes\n"

	assert.Equal(t, resultString, result.String())
}
