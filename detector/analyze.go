package detector

import (
	"bufio"
	"bytes"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/zj/linguist/internal/git"
)

type Analytics struct {
	repo    *git.Repository
	results Result
}

const numberOfWorkers = 4

func NewAnalyses(repo string) *Analytics {
	a := Analytics{repo: git.NewRepository(repo)}
	a.results = NewResult()
	return &a
}

func (a *Analytics) Analyze() {
	log.WithFields(log.Fields{"repo": a.repo}).Infoln("Starting analysis")
	blobsChan := make(chan Blob)
	go a.getBlobs(blobsChan)

	resultsChan := make(chan *Detection)
	go startWorkers(blobsChan, resultsChan)

	// TODO check if using a concurrent map is faster
	a.getResults(resultsChan)
	log.WithFields(log.Fields{"repo": a.repo}).Infoln("Done with analyses")
}

func (a *Analytics) getBlobs(out chan Blob) {
	buff, err := a.repo.RawTree("HEAD")
	if err != nil {
		log.WithFields(log.Fields{"repo": a.repo}).Errorf("Error retreiving the tree: %v", err)
		return
	}

	// Example line we expect:
	// 100644 blob 82efa4a1367795cebb4fee45dacf0ddc641d4a91   21012   Gemfile
	scanner := bufio.NewScanner(bytes.NewReader(buff))
	i := 0
	for scanner.Scan() {
		if i > 100000 { // Don't scan over 100k objects
			break
		}

		fields := strings.Fields(scanner.Text())
		oid, path := fields[2], fields[4]
		bytes, _ := strconv.ParseInt(fields[3], 10, 64)

		blob := NewBlob(a.repo, &path, &oid, bytes)

		if !blob.SkipAnalytics() {
			out <- *blob
		}

		i++
	}

	close(out)
}

func startWorkers(in chan Blob, out chan *Detection) {
	doneChan := make(chan bool, numberOfWorkers)

	for i := 0; i < numberOfWorkers; i++ {
		go func() {
			for blob := range in {
				detection := blob.Detect()
				if detection != nil {
					out <- detection
				}
			}
			doneChan <- true
		}()
	}

	for i := 0; i < numberOfWorkers; i++ {
		<-doneChan
	}

	close(out)
}

func (a *Analytics) getResults(in chan *Detection) {
	for detection := range in {
		a.results.Add(detection)
	}
}

func (a *Analytics) String() string {
	return a.results.String()
}
