package languages

type Language struct {
	Name         string
	Type         string
	Group        string
	Color        string
	Aliases      []string
	Extensions   []string
	Filenames    []string
	Interpreters []string
	MimeType     string
	TmScope      string
	AceMode      string
	Wrap         bool
	Searchable   bool
}

// Definitions comming from language_definition.go
//go:generate ruby ./generate_language_definitions

var (
	languagesByExtension   map[string][]*Language
	languagesByFilename    map[string][]*Language
	languagesByInterpreter map[string][]*Language
	languagesByMimeType    map[string][]*Language
)

func init() {
	languagesByExtension = make(map[string][]*Language, len(Languages))
	languagesByFilename = make(map[string][]*Language, len(Languages))
	languagesByInterpreter = make(map[string][]*Language, len(Languages))
	languagesByMimeType = make(map[string][]*Language, len(Languages))

	for _, lang := range Languages {
		for _, ext := range lang.Extensions {
			languagesByExtension[ext] = append(languagesByExtension[ext], lang)
		}

		for _, filename := range lang.Filenames {
			languagesByFilename[filename] = append(languagesByFilename[filename], lang)
		}

		for _, interpreter := range lang.Interpreters {
			languagesByInterpreter[interpreter] = append(languagesByInterpreter[interpreter], lang)
		}

		if lang.MimeType != "" {
			languagesByMimeType[lang.MimeType] = append(languagesByMimeType[lang.MimeType], lang)
		}
	}
}

func LanguageByFileName(key string) []*Language {
	return languagesByFilename[key]
}

func LanguageByExtension(key string) []*Language {
	return languagesByExtension[key]
}

func LanguageByInterpreter(key string) []*Language {
	return languagesByInterpreter[key]
}

func LanguageByMimeType(key string) []*Language {
	return languagesByInterpreter[key]
}
